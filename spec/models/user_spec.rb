RSpec.describe User, type: :model do
  let(:user) { build :user }

  context 'when email and username already exists' do
    let(:new_user) { build :user, email: user.email, username: user.username }

    before { user.save }

    subject { new_user }

    it { is_expected.to have(1).error_on :email }
    it { is_expected.to have(1).error_on :username }
  end
end
