FactoryGirl.define do
  factory :user do
    email { generate :email }
    username { Faker::Internet.user_name }
    password { generate :password }
    password_confirmation(&:password)

    first_name { Faker::Name.first_name }
    last_name  { Faker::Name.last_name }
  end
end
