FactoryGirl.define do
  sequence(:password) { |n| "aaBBcc#{n}$" }
  sequence(:email) { Faker::Internet.email }
end
