module FoxUsersServer
  module Api
    module V10
      class Base < Grape::API
        version FoxUsersServer::Settings.exposed_api_versions_for('v1.0')

        # mount V10::Registrations
      end
    end
  end
end
