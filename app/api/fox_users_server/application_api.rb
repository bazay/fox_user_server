module FoxUsersServer
  class ApplicationAPI < Grape:API
    # Do not fallback to Rails on 4xx errors
    cascade false

    format :json
    # error_formatter :json, JsonApiErrorFormatter

    # New API version should be mounted *before* old ones in order to override some endpoints
    mount Api::V10::Base

    desc 'App and api versions'
    get :version do
      api_version_hash
    end

    add_swagger_documentation hide_format: true,
                              api_version: 'v1.0',
                              mount_path: '/swagger_doc',
                              base_path: Figaro.env.rails_relative_url_root

    private

      def api_version_hash
        { app_version: FoxUsersServer::VERSION, api_version: 'v1.0' }
      end
  end
end
